/* eslint no-invalid-this: off, require-jsdoc: off */
jQuery.noConflict();
(function($) {
    $(function() {
        $('#ct-message__form').submit(function(e) {
            e.preventDefault();

            const btn = $(this).find('button[type=submit]');
            btn.prop('disabled', true);

            const textarea = $(this).find('textarea');

            const $this = $(this);
            $.ajax({
                url: $this.attr('action'),
                method: $this.attr('method'),
                data: $this.serialize()
            }).done(function(data) {
                toastr.success(data.success);
                textarea.val('');
            }).fail(function(xhr) {
                let msg = xhr.responseJSON.error ?
                    xhr.responseJSON.error : 'Message was not sent';
                toastr.error(msg);
            }).always(function() {
                btn.prop('disabled', false);
            });
        });
    });

    let getMessages = function() {
        let template = $('#ct-message-template');
        let messageContainer = $('#ct-dialog__messages-container');

        $.ajax({
            url: '/messages',
            method: 'GET'
        }).done(function(data) {
            console.log(data);
            $.each(data, function(n, msg) {
                if ($('#ct-message-' + msg.id).length == 0) {
                    let elm = template.clone().show();
                    elm.attr('id', 'ct-message-' + msg.id);
                    elm.find('.ct-media__address').html(msg.message);
                    messageContainer.append(elm);
                }
            });
        }).fail(function(xhr) {
            let msg = xhr.responseJSON.error ?
                xhr.responseJSON.error : 'Unable to fetch messages';
            toastr.error(msg);
        });
    };

    setInterval(getMessages, 3000);
})(jQuery);
