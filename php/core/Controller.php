<?php
namespace Framework\Core;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 */
class Controller {
    use SingletonTrait;
}