<?php
namespace Framework\Models;

use Doctrine\ORM\Mapping as ORM;
use Framework\Traits\CreatedModifiedTrait;
use Framework\Traits\IdentityTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="messages")
 * @ORM\Entity(repositoryClass="Framework\Repository\MessageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Message
{
    use IdentityTrait;
    use CreatedModifiedTrait;

    /**
     * @var null|string
     *
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     */
    private $user;

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param null|string $message
     *
     * @return Message
     */
    public function setMessage(?string $message): Message
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Message
     */
    public function setUser(User $user): Message
    {
        $this->user = $user;

        return $this;
    }
}
