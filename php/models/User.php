<?php
namespace Framework\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Framework\Traits\CreatedModifiedTrait;
use Framework\Traits\IdentityTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Framework\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User
{
    use IdentityTrait;
    use CreatedModifiedTrait;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var Message[]
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="user")
     */
    private $messages;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }


    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     *
     * @return User
     */
    public function setEmail(?string $email): User
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param Message $message
     * @return User
     */
    public function addMessage(Message $message): User
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
        }

        // TODO: See if this works
        /*foreach ($this->messages as $message) {
            $message->setUser($this);
        }*/

        return $this;
    }
}
