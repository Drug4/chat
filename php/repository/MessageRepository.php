<?php
namespace Framework\Repository;

use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    public function getMessages()
    {
        $qb = $this->createQueryBuilder('m');
        $qb
            ->select('m, u')
            ->join('m.user', 'u')
            ->orderBy('m.createdAt', 'asc')
            ->setMaxResults(4)
        ;

        return $qb->getQuery()->getResult();
    }
}