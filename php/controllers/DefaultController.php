<?php
namespace Framework\Controllers;

use Doctrine\ORM\EntityManager;
use Framework\Core\Controller;
use Framework\Core\Doctrine;
use Framework\Models\Message;
use Framework\Models\User;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;

defined('BASEPATH') OR exit('No direct script access allowed');

class DefaultController extends Controller
{
    public function indexAction()
    {
        require_once BASEPATH . 'templates/index.php';
    }

    public function getMessages()
    {
        header('Content-Type: application/json');

        /** @var EntityManager $em */
        $em = Doctrine::getEntityManager();
        $messages = $em->getRepository(Message::class)->getMessages();

        $jsonData = array_map(function (Message $msg) {
            /** @var Message $msg */
            $std = new \stdClass();
            $std->id = $msg->getId();
            $std->message = $msg->getMessage();
            $std->user = $msg->getUser()->getId();

            return $std;
        }, $messages);

        echo json_encode($jsonData);
    }

    public function postMessage()
    {
        header('Content-Type: application/json');

        $validator = v::key('message', v::notEmpty()->stringType()->prnt()->length(12, 1024)->setName('Message'));

        try {
            $validator->assert($_POST);
            /** @var EntityManager $em */
            $em = Doctrine::getEntityManager();

            $message = $_POST['message'] ?? null;
            $entity = (new Message())->setMessage($message);

            $user = new User();
            $user->setEmail('test@gmail.com');
            $em->persist($user);

            $entity->setUser($user);

            $em->persist($entity);
            $em->flush();

            http_response_code(200);
            echo json_encode(['success' => 'Message sent']);
        }
        catch (NestedValidationException $e) {
            http_response_code(400);
            echo json_encode(['error' => $e->getFullMessage()]);
        }
    }
}