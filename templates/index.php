<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="vendor.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<div class="ct-container">
    <div class="ct-contacts">
        <div class="ct-me">
            <div class="ct-avatar ct-avatar--large">
                <img src="img/avatar_circle_blue_512dp.png" alt="" class="ct-avatar__image">
            </div>
            <div class="ct-me__username">
                Jarred Delport
            </div>
            <div class="ct-me__email">
                dm@dailyui13.done
            </div>
        </div>
        <div class="ct-contact-container">
            <div class="ct-contact ct-contact--hover">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="img/avatar_circle_blue_512dp.png" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Eula Shelton
                    </div>
                    <div class="ct-contact__user-last-message">Hello, did you see my...</div>
                    <div class="ct-contact__user-last-message-time">34 mins ago</div>
                </div>
            </div>
            <div class="ct-contact ct-contact--active">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="img/avatar_circle_blue_512dp.png" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Marcus Little
                    </div>
                    <div class="ct-contact__user-last-message">
                        is Typing...
                    </div>
                    <div class="ct-contact__user-unread-message-number">
                        <span>2</span>
                    </div>
                </div>
            </div>
            <div class="ct-contact ct-contact--hover">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="img/avatar_circle_blue_512dp.png" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Eula Shelton
                    </div>
                    <div class="ct-contact__user-last-message">Hello, did you see my...</div>
                    <div class="ct-contact__user-last-message-time">34 mins ago</div>
                </div>
            </div>
            <div class="ct-contact ct-contact--active">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="img/avatar_circle_blue_512dp.png" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Marcus Little
                    </div>
                    <div class="ct-contact__user-last-message">
                        is Typing...
                    </div>
                    <div class="ct-contact__user-unread-message-number">
                        <span>2</span>
                    </div>
                </div>
            </div>
            <div class="ct-contact ct-contact--hover">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Eula Shelton
                    </div>
                    <div class="ct-contact__user-last-message">Hello, did you see my...</div>
                    <div class="ct-contact__user-last-message-time">34 mins ago</div>
                </div>
            </div>
            <div class="ct-contact ct-contact--active">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Marcus Little
                    </div>
                    <div class="ct-contact__user-last-message">
                        is Typing...
                    </div>
                    <div class="ct-contact__user-unread-message-number">
                        <span>2</span>
                    </div>
                </div>
            </div>
            <div class="ct-contact ct-contact--hover">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Eula Shelton
                    </div>
                    <div class="ct-contact__user-last-message">Hello, did you see my...</div>
                    <div class="ct-contact__user-last-message-time">34 mins ago</div>
                </div>
            </div>
            <div class="ct-contact ct-contact--active">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Marcus Little
                    </div>
                    <div class="ct-contact__user-last-message">
                        is Typing...
                    </div>
                    <div class="ct-contact__user-unread-message-number">
                        <span>2</span>
                    </div>
                </div>
            </div>
            <div class="ct-contact ct-contact--hover">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Eula Shelton
                    </div>
                    <div class="ct-contact__user-last-message">Hello, did you see my...</div>
                    <div class="ct-contact__user-last-message-time">34 mins ago</div>
                </div>
            </div>
            <div class="ct-contact ct-contact--active">
                <div class="ct-contact__avatar-container">
                    <div class="ct-avatar ct-avatar--large">
                        <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                </div>
                <div class="ct-contact__user-info-container">
                    <div class="ct-contact__user-name">
                        Marcus Little
                    </div>
                    <div class="ct-contact__user-last-message">
                        is Typing...
                    </div>
                    <div class="ct-contact__user-unread-message-number">
                        <span>2</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ct-dialog">
        <div class="ct-dialog__top-bar">
            <div class="ct-dialog__interlocutor">
                <div class="ct-avatar ct-avatar--medium">
                    <div class="ct-avatar__user-status ct-avatar__user-status--online"></div>
                    <img src="" alt="" class="ct-avatar__image">
                </div>
                <div class="ct-dialog__interlocutor-name">
                    Marcus Little
                </div>
            </div>
            <div class="ct-dialog__actions">
                <div class="ct-dialog__action">
                    <div class="ct-icon ct-icon--large ct-icon--grey ct-icon--hover-violet">
                        <img src="" alt="svg" class="ct-icon__svg">
                    </div>
                </div>
                <div class="ct-divider-vertical">
                    <img src="" alt="svg" class="ct-divider-vertical__icon">
                </div>
                <div class="ct-dialog__action">
                    <div class="ct-icon ct-icon--large ct-icon--grey ct-icon--hover-violet">
                        <img src="" alt="svg" class="ct-icon__svg">
                    </div>
                </div>
                <div class="ct-divider-vertical">
                    <img src="" alt="svg" class="ct-divider-vertical__icon">
                </div>
                <div class="ct-dialog__action">
                    <div class="ct-icon ct-icon--large ct-icon--grey ct-icon--hover-violet">
                        <img src="" alt="svg" class="ct-icon__svg">
                    </div>
                </div>
            </div>
        </div>
        <div id="ct-dialog__messages-container" class="ct-dialog__messages-container">
            <!--<div class="ct-message ct-message--my">
                <div class="ct-message__user">
                    <div class="ct-avatar ct-avatar--small">
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                    <div class="ct-message__time">15:47</div>
                </div>
                <div class="ct-message__container">
                    <div class="ct-message__text">
                        Lorem ipsum dolor. <span class="ct-icon ct-icon--emoticon"><img src="" alt="svg"></span>
                    </div>
                </div>
            </div>-->
            <div id="ct-message-template" class="ct-message" style="display: none">
                <div class="ct-message__unread"></div>
                <div class="ct-message__container">
                    <div class="ct-media">
                        <div class="ct-media__image-container">
                            <img src="" alt="image" class="">
                        </div>
                        <div class="ct-media__info-container">
                            <div class="ct-media__name">Kwaal Restaurant</div>
                            <div class="ct-media__address"></div>
                            <div class="ct-media__rating">
                                <div class="ct-icon ct-icon--small">

                                </div>
                                <div class="ct-media__rating-quantity">1.4k Ratings</div>
                            </div>
                            <div class="ct-media__button">
                                RSVP NOW
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ct-message__user">
                    <div class="ct-avatar ct-avatar--small">
                        <img src="" alt="" class="ct-avatar__image">
                    </div>
                    <div class="ct-message__time"></div>
                </div>
            </div>
        </div>
        <div class="ct-dialog__send-bar">
            <form id="ct-message__form" method="post" action="/post">
                <textarea name="message" cols="30" rows="10"></textarea>
                <button type="submit">Send</button>
            </form>
        </div>
    </div>
</div>

<script src="vendor.js"></script>
<script src="scripts.js"></script>
</body>
</html>
